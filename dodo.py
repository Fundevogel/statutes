from os.path import exists, join
from doit import get_var
from helpers import create_path, int2roman, load_yaml, replace_references


###
# CONFIG (START)
#

VERSION = '0.1.0'

DOIT_CONFIG = {
    'action_string_formatting': 'old',
    'verbosity': 2,
}

#
# CONFIG (END)
###


###
# VARIABLES (START)
#

# Directories
dist = 'build'
assets = 'assets'
styles_dir = join(assets, 'styles')
fonts_dir = join(assets, 'fonts')

# Files
file = get_var('file', 'satzung')
md_file = join(dist, '%s.md' % file)
meta_file = join('config', 'metadata.yml')

# Metadata
metadata = load_yaml(meta_file)

# Generic `pandoc` command
command = [
    'pandoc {}'.format(md_file),
    # '-f gfm',
    '--metadata-file {}'.format(meta_file),
    '--toc',
    '--data-dir assets',
]

#
# VARIABLES (END)
###


###
# GROUPS (START)
#

def task_all():
    """
    Creates all formats
    """

    return {
        'actions': None,
        'task_dep': [
            'build',
            'epub',
            'html',
            'odt',
            'pdf',
        ]
    }

#
# GROUPS (END)
###


###
# TASKS (START)
#

def task_build():
    """
    Builds Markdown document
    """

    # Build path to currently selected module
    module = 'modules/%s' % get_var('src', 'satzung')

    if not exists(module):
        raise Exception('Module does not exist: "{}"'.format(module))

    # Select & load its ..
    # (1) .. structure
    setup_file = join(module, 'setup.yml')
    data = load_yaml(setup_file)

    # (2) .. variables
    vars_file = join(module, 'vars.yml')
    variables = load_yaml(vars_file)

    # Fetch source files
    src_files = ['{}/src/{}.md'.format(module, file) for section in data for file in section['files']]


    def build() -> None:
        content = []
        index = {}

        # Count articles
        count = 0

        # Use section count
        section_count = 0

        for section in data:
            # Increase article count
            section_count += 1

            # Print headline
            content.append('## {}. {}'.format(int2roman(section_count), section['title']))
            content.append('\n')
            content.append('\n')

            for slug in section['files']:
                # Increase article count
                count += 1

                # Add article to index
                index[slug] = count

                # Save article contents
                # (1) Get file path
                file_path = join(module, 'src', '{}.md'.format(slug))

                # (2) Extract lines
                with open(file_path, 'r') as input_file:
                    content += [line + '\n' for line in input_file.read().splitlines()]

                content.append('\n')

        # Create file destination (if necessary)
        create_path(md_file)

        # Count articles
        article_count = 0

        is_comment = False

        with open(md_file, 'w') as file:
            for line in content:
                # Look for placeholders article count placeholders, otherwise ..
                if line.strip()[:2] == '$$':
                    # Increase article count
                    article_count += 1

                    # Replace placeholder with markdown heading, section sign & article count
                    file.write(line.replace('$$', '### § {}'.format(str(article_count))))

                    # Proceed to next line
                    continue

                # Remove comments
                if line.strip()[:4] == '<!--':
                    # (1) Enable comment removal on comment start
                    is_comment = True

                if is_comment:
                    # (2) Disable comment removal on comment end
                    if '-->' in line:
                        is_comment = False

                    # (3) Skip comments
                    continue

                # Print newlines
                if line == '\n' or line == '':
                    file.write('\n')

                    # Proceed to next line
                    continue

                # Replace internal variables & norm references
                file.write(replace_references(line, index, variables))

    return {
        'file_dep': [setup_file, vars_file, meta_file] + src_files,
        'actions': [(build)],
        'targets': [md_file],
    }


def task_epub():
    """
    Converts Markdown to ePUB (electronic publication)
    """

    epub_css = join(styles_dir, 'epub.css')
    epub_file = join(dist, '%s.epub' % file)
    epub_fonts = join(fonts_dir, '*.ttf')

    epub_command = command + [
        # TODO: Check out epub3
        '-t epub',
        '--css {}'.format(epub_css),
        # TODO: Make fonts work
        '--epub-embed-font=\'{}\''.format(epub_fonts),
        '-o {}'.format(epub_file),
    ]

    return {
        'file_dep': [md_file, meta_file, epub_css],
        'actions': [' '.join(epub_command)],
        'targets': [epub_file],
    }


def task_html():
    """
    Converts Markdown to HTML (Hypertext Markup Language)
    """

    html_css = join(styles_dir, 'style.css')
    html_file = join(dist, '%s.html' % file)

    html_command = command + [
        '-t html',
        '--self-contained',
        '--css {}'.format(html_css),
        '--metadata title="{}"'.format(metadata['title'][0]['text']),
        '--metadata subtitle="{}"'.format(metadata['title'][1]['text']),
        '--section-divs',
        '-o {}'.format(html_file),
    ]

    return {
        'file_dep': [md_file, html_css],
        'actions': [' '.join(html_command)],
        'targets': [html_file],
    }


def task_odt():
    """
    Converts Markdown to ODT (OpenDocument Text)
    """

    odt_file = join(dist, '%s.odt' % file)

    odt_command = command + [
        '-t odt',
        '-o {}'.format(odt_file),
    ]

    return {
        'file_dep': [md_file],
        'actions': [' '.join(odt_command)],
        'targets': [odt_file],
    }


def task_pdf():
    """
    Converts Markdown to PDF (Portable Document Format)
    """

    pdf_file = join(dist, '%s.pdf' % file)

    pdf_command = command + [
        '--pdf-engine=xelatex',
        '--template pdf.latex',
		# '-V geometry:"top=1.5cm, bottom=2.5cm, left=1.5cm, right=1.5cm"',
		# '-V geometry:a4paper',
        # '-V documentclass=scrbook',
        # '-V indent',
        # '-V subparagraph',
        # '--top-level-division=part',
        # '--metadata pagetitle="{}"'.format(title),
        '-o {}'.format(pdf_file),
    ]

    return {
        'file_dep': [md_file],
        'actions': [' '.join(pdf_command)],
        'targets': [pdf_file],
    }

#
# TASKS (END)
###
