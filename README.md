# Satzung der Fundevogel eG (in Gründung)

This repository houses everything that is needed to build our statute (using `pandoc` under the hood), which includes the individual articles as well as various assets, like styles, images etc.

The build process features a high level of configurability, such as easily reordering or replacing articles, maintaining different statutes at the same time, .. and offers many output options, such as Markdown, PDF, HTML, ePub & ODT.


## Getting started

Simply create a virtual environment using `virtualenv` & install its dependencies:

```bash
# Set up & activate virtualenv
virtualenv -p python3 venv

# shellcheck disable=SC1091
source venv/bin/activate

# Install dependencies
pip install -r requirements.txt

# Create output directory
mkdir -p build
```

Now, using `doit list` shows all available commands:

```text
$ doit list
all     Creates all formats
build   Builds Markdown document
epub    Converts Markdown to ePUB (electronic publication)
html    Converts Markdown to HTML (Hypertext Markup Language)
odt     Converts Markdown to ODT (OpenDocument Text)
pdf     Converts Markdown to PDF (Portable Document Format)
```

As you can see, generating the statute in PDF format can be achieved with `doit pdf`, an aesthetically pleasing ePub file with `doit epub` - you know the drill ..


## Template

All articles look something like this:

```
$$ Heading

<!-- This is a comment -->
<!-- Comments are removed,
regardless
how long they are
-->

(1) Some text
(2) Some more text

  * This is a list
  * A very simple one

<!-- The following reference will be replaced with eg '§ 1' -->
(3) And even more text, see {{ path/to/article }} for more information

  a) more sophisticated lists ..
  b) .. look like this
```

.. where `$$` becomes `§ 1` for the first article, `§ 2` for the second article and so on.


## Roadmap

- [x] Add metadata file
- [ ] Improve styling
- [ ] Add fonts, remove `texlive-libertine` dependency
- [x] Fix indentation of lists, articles without paragraph numbers, ..


## Requirements

This library uses `pandoc` for all transformations, along with a little Python magic. Therefore, the following packages (or their equivalents for your OS) are required:

- pandoc
- texlive
- texlive-libertine
- texlive-ctablestack


## Credits

[`pandoc`](https://pandoc.org) by [John MacFarlane](https://johnmacfarlane.net) is just awesome.
