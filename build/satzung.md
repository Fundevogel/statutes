## I. Grundsätzliches

### § 1 Firma und Sitz

(1) Die Firma der Genossenschaft lautet Fundevogel eG.

(2) Die Genossenschaft hat ihren Sitz in Freiburg.

### § 2 Zweck und Gegenstand

(1) Die Genossenschaft bezweckt die wirtschaftliche Förderung und Betreuung der Mitglieder mittels gemeinschaftlichen Geschäftsbetriebes; dabei steht Kinder- und Jugendliteratur im Mittelpunkt.

(2) Gegenstand der Genossenschaft ist der Erwerb und Vertrieb von Druckerzeugnissen, Bild-, Ton- und sonstigen Datenträgern sowie Medien aller Art. Die Genossenschaft verwirklicht ihren satzungsmäßigen Zweck insbesondere durch

  a) Fortführung der Freiburger Kinder- und Jugendbuchhandlung Fundevogel,
  b) Durchführung von Veranstaltungen mit Autor:innen und Verlagen,
  d) Herausgabe von Empfehlungslisten mit ausgewählten Neuerscheinungen,
  c) Angebote der Leseförderung und -forderung junger Menschen sowie
  e) Fortbildungsmaßnahmen im Bereich der frühkindlichen Bildung.

(3) Die Genossenschaft strebt zur Erfüllung der vorgenannten Ziele die Zusammenarbeit und Vernetzung mit gleichgesinnten Kooperationspartner:innen an. Außerdem unterstützt die Genossenschaft die Mitglieder bei der Weiterbildung in den zuvor genannten Bereichen.

(4) Die Genossenschaft ist berechtigt, andere Unternehmen zu errichten und zu erwerben sowie sich an anderen Unternehmen zu beteiligen, wenn dies der Förderung der Mitglieder dient. Sie ist ferner berechtigt, Zweigniederlassungen zu errichten.

(5) Die Ausdehnung des Geschäftsbetriebes auf Nichtmitglieder ist zugelassen.

## II. Mitliedschaft

### § 3 Erwerb der Mitgliedschaft

(1) Die Mitgliedschaft können erwerben:

  a) natürliche Personen,
  b) Personengesellschaften,
  c) juristische Personen des privaten oder öffentlichen Rechts.

(2) Aufnahmefähig ist nur, wer die Voraussetzungen für die Inanspruchnahme der Einrichtungen der Genossenschaft erfüllt oder dessen Mitgliedschaft im Interesse der Genossenschaft liegt. Aufnahmefähig ist nicht, wer bereits Mitglied einer anderen Vereinigung ist, die im Wesentlichen gleichartige Geschäfte betreibt, oder wer derartige Geschäfte selbst betreibt oder betreiben lässt.

(3) Die Mitgliedschaft wird erworben durch

  a) eine von dem Beitretenden zu unterzeichnende unbedingte Erklärung des Beitritts und
  b) die Zulassung durch das Betriebsplenum.

Lehnt der Vorstand die Aufnahme ab, so steht der abgewiesenen Person innerhalb eines Monats nach der Benachrichtigung das Recht der Berufung an den Aufsichtsrat zu, der endgültig entscheidet.

### § 4 Rechte der Mitglieder

(1) Alle Mitglieder haben, unabhängig von ihren Geschäftsanteilen, eine Stimme in der Generalversammlung und der Online-Generalversammlung.

(2) Jedes Mitglied hat das Recht, die Einrichtungen der Genossenschaft nach Maßgabe der dafür getroffenen Bestimmungen zu benutzen und an der Gestaltung der Genossenschaft mitzuwirken.

### § 5 Pflichten der Mitglieder

Jedes Mitglied ist verpflichtet,

  a) die Interessen der Genossenschaft zu wahren und den Bestimmungen des Genossenschaftsgesetzes, der Satzung und den Beschlüssen der Generalversammlung nachzukommen,
  b) die Einzahlung auf den Geschäftsanteil zu leisten oder auf weitere Geschäftsanteile gemäß § 6 Abs. 3 zu leisten,
  c) Adressenänderungen innerhalb von drei Wochen dem Vorstand mitzuteilen und
  d) interne Informationen und Vorgänge sowie sonstige Angelegenheiten, die der Genossenschaft erheblichen Schaden zufügen können, nicht an unbeteiligte Dritte weiterzugeben.

### § 6 Geschäftsanteil

(1) Der Geschäftsanteil beträgt XY Euro und ist bei Eintritt an die Genossenschaft zu zahlen. Jedes Mitglied, das juristische Person ist, sollte wenigstens fünf Geschäftsanteile erwerben.

(2) Sacheinlagen können mit Zustimmung des Betriebsplenums als Einzahlung auf den Geschäftsanteil zugelassen werden.

(3) Ein Mitglied kann sich mit weiteren Geschäftsanteilen an der Genossenschaft betei­ligen; für den Erwerb gilt § 3 entsprechend. Falls ein Mitglied nach dem Erwerb weiterer Geschäftsanteile 10 % aller Geschäftsanteile besitzen würde, bedarf der Erwerb der Zustimmung des Aufsichtsrates.

### § 7 Nachschusspflicht

Eine Nachschusspflicht der Mitglieder besteht nicht.

### § 8 Beendigung der Mitgliedschaft

Die Mitgliedschaft endet durch

  a) Kündigung (§ 9 Abs. 1),
  b) Ausschluss (§ 10).
  c) Übertragung des Geschäftsguthabens (§ 11 Abs. 1),
  d) Tod (§ 12 Abs. 1) oder Insolvenz eines Mitglieds (§ 12 Abs. 2),
  e) Auflösung einer juristischen Person oder Personengesellschaft (§ 77a GenG) oder

### § 9 Kündigung

(1) Jedes Mitglied kann seine Mitgliedschaft zum Schluss eines Geschäftsjahres unter Einhaltung einer Frist von sechs Monaten schriftlich kündigen.

(2) Ein Mitglied, das mit mehreren Geschäftsanteilen beteiligt ist, kann die Beteiligung mit einem oder mehreren seiner weiteren Geschäftsanteile zum Schluss eines Geschäftsjahres unter Einhaltung einer Frist von neun Monaten schriftlich kündigen.

### § 10 Ausschluss

(1) Ein Mitglied kann aus der Genossenschaft zum Schluss des Geschäftsjahres ausgeschlossen werden, wenn die Voraussetzungen für die Aufnahme in die Genossenschaft nicht vorhanden waren oder nicht mehr vorhanden sind oder wenn sich das Verhalten des Mitglieds mit den Belangen der Genossenschaft nicht vereinbaren lässt. Dies ist insbesondere dann der Fall, wenn es

  a) trotz schriftlicher Aufforderung unter Androhung des Ausschlusses den satzungsmäßigen oder sonstigen der Genossenschaft gegenüber bestehenden Verpflichtungen nicht nachkommt,
  b) unrichtige Jahresabschlüsse oder Vermögensübersichten einreicht oder sonst unrichtige oder unvollständige Erklärungen über seine rechtlichen oder wirtschaftlichen Verhältnisse abgibt,
  c) durch Nichterfüllung seiner Verpflichtungen gegenüber der Genossenschaft diese schädigt oder geschädigt hat,
  d) ein eigenes, mit der Genossenschaft im Wettbewerb stehendes Unternehmen betreibt oder sich an einem solchen beteiligt oder wenn ein mit der Genossenschaft im Wettbewerb stehendes Unternehmen sich an dem Unternehmen des Mitglieds beteiligt oder
  e) unter der der Genossenschaft bekannt gegebenen Anschrift dauernd nicht erreichbar ist.

(2) Über den Ausschluss entscheidet der Vorstand mit Zustimmung des Aufsichtsrates. Mitglieder des Vorstandes und des Aufsichtsrates können jedoch nur durch Beschluss der Generalversammlung ausgeschlossen werden.

(3) Vor der Beschlussfassung ist dem Auszuschließenden Gelegenheit zu geben, sich zu dem beabsichtigten Ausschluss zu äußern. Hierbei sind ihm die wesentlichen Tatsachen, auf denen der Ausschluss beruhen soll, sowie der satzungsmäßige Ausschließungsgrund mitzuteilen.

(4) Die ausgeschlossene Person kann, wenn nicht die Generalversammlung den Ausschluss beschlossen hat, innerhalb eines Monats seit der Absendung des Briefes Beschwerde beim Aufsichtsrat einlegen; in diesem Fall entscheidet die Generalversammlung über den Ausschluss. Legt die ausgeschlossene Person nicht fristgerecht Beschwerde ein, ist der ordentliche Rechtsweg ausgeschlossen.

### § 11 Übertragung des Geschäftsguthabens

(1) Ein Mitglied kann jederzeit, auch im Laufe des Geschäftsjahres, sein Geschäftsguthaben durch schriftlichen Vertrag einem anderen übertragen und hierdurch aus der Genossenschaft ohne Auseinandersetzung ausscheiden, sofern die erwerbende Person an seiner Stelle Mitglied wird. Ist die erwerbende Person bereits Mitglied, so ist die Übertragung des Geschäftsguthabens nur zulässig, sofern sein bisheriges Geschäftsguthaben nach Zuschreibung des Geschäftsguthabens des Veräußerers den zulässigen Gesamtbetrag der Geschäftsanteile, mit denen die erwerbende Person beteiligt ist oder sich beteiligt, nicht übersteigt.

(2) Ein Mitglied kann sein Geschäftsguthaben, ohne aus der Genossenschaft auszuscheiden, teilweise übertragen und damit die Anzahl seiner Geschäftsanteile verringern. Absatz 1 gilt entsprechend.

(3) Die Übertragung des Geschäftsguthabens bedarf außer in den Fällen des § 76 Absatz 2 Genossenschaftsgesetz der Zustimmung des Vorstands.

### § 12 Tod und Insolvenz

(1) Im Falle des Todes eines Mitglieds wird dessen Mitgliedschaft in der Genossenschaft durch dessen Erb:in fortgesetzt. Für den Fall der Beerbung durch mehrere Erb:innen endet die Mitgliedschaft, wenn sie nicht bis zum Ablauf des auf den Erbfall folgenden Jahres eine:r Miterb:in allein überlassen worden ist. Mehrere Erb:innen können das Stimmrecht in der Generalversammlung nur durch einen gemeinschaftliche:n Vertreter:in ausüben.

(2) Wird über das Vermögen eines Mitglieds ein Insolvenzverfahren eröffnet oder die Eröffnung eines Insolvenzverfahrens mangels Masse abgelehnt, so endet die Mitgliedschaft mit dem Schluss des Geschäftsjahres, in dem das Insolvenzverfahren eröffnet oder die Eröffnung mangels Masse abgelehnt wurde.

### § 13 Auseinandersetzung

(1) Für die Auseinandersetzung zwischen dem ausgeschiedenen Mitglied und der Genossenschaft ist der festgestellte Jahresabschluss maßgebend; Verlustvorträge sind nach dem Verhältnis der Geschäftsanteile zu berücksichtigen. Im Fall der Übertragung des Geschäftsguthabens (§ 11) findet eine Auseinandersetzung nicht statt.

(2) Dem ausgeschiedenen Mitglied ist das Auseinandersetzungsguthaben binnen sechs Monaten nach dem Ausscheiden auszuzahlen. Die Genossenschaft ist berechtigt, bei der Auseinandersetzung die ihr gegen das ausgeschiedene Mitglied zustehenden fälligen Forderungen gegen das auszuzahlende Guthaben aufzurechnen. Auf die Rücklagen und das sonstige Vermögen der Genossenschaft hat das Mitglied keinen Anspruch.

(3) Der Genossenschaft haftet das Auseinandersetzungsguthaben des Mitglieds als Pfand für einen etwaigen Ausfall, insbesondere im Insolvenzverfahren des Mitglieds.

(4) Die Absätze 1 bis 3 gelten entsprechend für die Auseinandersetzung bei der Kündigung einzelner Geschäftsanteile (§ 9 Abs. 2).


## III. Organe

### § 14 Vorstand

(1) Der Vorstand besteht aus mindestens zwei, höchstens sechs Mitgliedern und arbeitet ehrenamtlich.

(2) Je zwei Mitglieder des Vorstands vertreten gemeinschaftlich die Genossenschaft gerichtlich und außergerichtlich. Die Genossenschaft kann auch durch ein Vorstandsmitglied in Gemeinschaft mit einem Prokuristen gesetzlich vertreten werden.

(3) Die Vorstandsmitglieder werden vom Aufsichtsrat aus der Mitte der momentan Beschäftigten der Genossenschaft bestellt und abberufen. Den Vorsitzenden des Vorstands und dessen Stellvertreter wählt der Vorstand aus seiner Mitte.

(4) Die Amtzeit der Vorstandsmitglieder beträgt drei Jahre.

(5) Der Vorstand bedarf im Innenverhältnis der Zustimmung des Betriebsplenums (§ 18) für

  a) den Abschluss von Verträgen mit wiederkehrenden Verpflichtungen mit einer Laufzeit von mehr als zwei Jahren bzw. einer jährlichen Belastung von mehr als 2.000 Euro,
  b) Investitionen bzw. die Aufnahme von Krediten mit einem Gesamtvolumen von mehr als 5.000 Euro,
  c) die Aufnahme, Übertragung oder Aufgabe eines wesentlichen Geschäftsbereichs, soweit nicht die Generalversammlung zuständig ist,
  d) den Beitritt zu und Austritt aus Verbänden und sonstigen Vereinigungen sowie
  f) die Erteilung von Prokura.

### § 15 Aufsichtsrat

(1) Der Aufsichtsrat besteht aus mindestens drei, höchstens neun Mitgliedern, die von der Generalversammlung gewählt werden. Er wird einzeln vertreten vom Vorsitzenden oder dessen Stellvertreter.

(2) Die Wahl der Aufsichtsratsmitglieder erfolgt mit der Mehrheit der Stimmen der anwesenden Mitglieder; Stimmenthaltungen gelten als ungültig und werden nicht mitgezählt. Erhalten mehr Bewerber die erforderliche Mehrheit, als Sitze im Aufsichtsrat zu besetzen sind, so sind die Bewerber mit den meisten Stimmen gewählt.

(3) Das Amt eines Aufsichtsratsmitglieds beginnt mit dem Schluss der Generalversammlung, die die Wahl vorgenommen hat, und endet am Schluss der Generalversammlung, die für das dritte Geschäftsjahr nach der Wahl stattfindet. Hierbei wird das Geschäftsjahr, in welchem das Aufsichtsratsmitglied gewählt wird, mitgerechnet. Die Generalversammlung kann für alle oder einzelne Aufsichtsratsmitglieder eine kürzere Amtsdauer bestimmen. Wiederwahl ist zulässig.

(4) Die Wahl zum Mitglied des Aufsichtsrats kann vor dem Ende der Amtszeit durch die Generalversammlung widerrufen werden. Der Beschluss bedarf einer Mehrheit von drei Vierteln der abgegebenen Stimmen.

(5) Der Aufsichtsrat wählt im Anschluss an jede Wahl von Aufsichtsratsmitgliedern aus seiner Mitte einen Vorsitzenden sowie einen Stellvertreter.

(6) Der Aufsichtsrat ist beschlussfähig, wenn mehr als die Hälfte seiner Mitglieder, darunter der Vorsitzende oder sein Stellvertreter, anwesend ist. Eine Beschlussfassung ist in dringenden Fällen auch ohne Einberufung einer Sitzung im Wege schriftlicher Abstimmung oder durch entsprechende Fernkommunikationsmedien zulässig, wenn der Vorsitzende des Aufsichtsrats oder sein Stellvertreter eine solche Beschlussfassung veranlasst und kein Mitglied des Aufsichtsrats diesem Verfahren widerspricht.

(7) Der Aufsichtsrat hat den Vorstand bei dessen Geschäftsführung zu überwachen und sich zu diesem Zweck über die Angelegenheiten der Genossenschaft zu unterrichten. Er kann jederzeit Berichterstattung vom Vorstand verlangen und selbst oder durch einzelne von ihm zu bestimmende Mitglieder die Bücher und Schriften der Genossenschaft sowie den Kassenbestand und die Bestände an Wertpapieren, Handelspapieren und Waren einsehen und prüfen. Auch ein einzelnes Mitglied des Aufsichtsrats kann Auskünfte, jedoch nur an den Aufsichtsrat, verlangen.

### § 16 Generalversammlung

(1) Die Generalversammlung wird vom Vorstand durch unmittelbare Benachrichtigung sämtlicher Mitglieder in Textform einberufen. Bei der Einberufung ist die Tagesordnung bekannt zu machen. Die Einladung muss mindestens zwei Wochen, Ergänzungen und Änderungen der Tagesordnung müssen mindestens eine Woche vor der Generalversammlung erfolgen. Die Mitteilungen gelten als zugegangen, wenn sie zwei Werktage vor Beginn der Frist abgesendet worden sind.

(2) Jede ordnungsgemäß einberufene Generalversammlung ist (unabhängig von der Zahl der Teilnehmer:innen) beschlussfähig.

(3) Jedes Mitglied hat eine Stimme.

(4) Die Generalversammlung bestimmt die Versammlungsleitung auf Vorschlag des Aufsichtsrates.

(5) Abstimmungen und Wahlen erfolgen in der Generalversammlung durch offene Abstimmung.

(6) Im Rahmen der Regelungen in § 43 Abs. 7 GenG haben die Mitglieder ein Recht auf Online-Teilnahme an der Generalversammlung und Beteiligung an der Fassung der Beschlüsse in elektronischer Form. Hierbei ist zu gewährleisten, dass die Identität des Mitglieds zweifelsfrei überprüft wird. Über die Möglichkeit der Online-Teilnahme ist in der Ladung zur Generalversammlung hinzuweisen. Sofern Beschlussfassungen geheim durchgeführt werden, sind geeignete Maßnahmen zu ergreifen, um eine anonymisierte Stimmabgabe bei Online-Teilnahme zu gewährleisten.

(7) Die Generalversammlung darf keine Gewinnverteilung an die Mitglieder beschließen.

(8) Beschlüsse werden gemäß § 47 GenG protokolliert.

### § 17 Mehrheitserfordernisse

(1) Entscheidungen der Genossenschaft sollen stets im Bemühen um Konsens erfolgen.

(2) Die Generalversammlung beschließt mit der Mehrheit der abgegebenen Stimmen (einfache Stimmenmehrheit), soweit keine größere Mehrheit bestimmt ist; Stimmenthaltungen bleiben unberücksichtigt. Gibt es bei einer Wahl mehr Bewerber:innen als Mandate vorhanden sind, so hat jede:r Wahlberechtigte so viele Stimmen, wie Mandate zu vergeben sind. Es sind diejenigen Bewerber:innen gewählt, die die meisten Stimmen auf sich vereinigen (relative Mehrheit).

(3) Eine Mehrheit von drei Vierteln der gültig abgegebenen
Stimmen ist insbesondere in folgenden Fällen erforderlich:

  a) Änderung der Satzung;
  b) Ausschluss von Vorstands- und Aufsichtsratsmitgliedern aus der Genossenschaft;
  c) Austritt aus genossenschaftlichen Verbänden;
  d) Verschmelzung oder Auflösung der Genossenschaft sowie die Fortsetzung der Genossenschaft nach beschlossener Auflösung.

(4) Ein Beschluss über die Änderung der Rechtsform bedarf
der Mehrheit von neun Zehnteln der gültig abgegebenen
Stimmen.

(5) Bei der Beschlussfassung über die Auflösung, die Änderung der Rechtsform und bei der Wahl und Abberufung des Vorstands müssen über die gesetzlichen Vorschriften hinaus 60% aller Mitglieder anwesend sein. Wenn diese Mitgliederzahl in der Versammlung nicht erreicht ist, ist jede weitere Versammlung ohne Rücksicht auf die Zahl der erschienenen Mitglieder innerhalb desselben Geschäftsjahres beschlussfähig.

(6) Die Absätze 3 und 4 können nur mit der in Absatz 3 genannten Mehrheit geändert werden.

### § 18 Betriebsplenum

Das Betriebsplenum ist eine Versammlung von mehr als der Hälfte der momentan Beschäftigten der Genossenschaft.

## IV. Wirtschaft

### § 19 Geschäftsführung

(1) Das Geschäftsjahr ist das Kalenderjahr.

(2) Der Vorstand stellt innerhalb von fünf Monaten nach Ende des Geschäftsjahres den Jahresabschluss und ggf. den Lagebericht für das vergangene Jahr Geschäftsjahr auf, und legt sie dem Aufsichtsrat zur Prüfung vor.

(3) Jahresabschluss und ggf. Lagebericht werden auf der Generalversammlung mit den Bemerkungen des Aufsichtsrates vorgelegt. Der Aufsichtsrat berichtet auf der Generalversammlung über seine Prüfung des Jahresabschlusses und des Lageberichts.

(4) Zwei Wochen vor der Generalversammlung werden der Jahresabschluss, ggf. Lagebericht
und der Bericht des Aufsichtsrates in den Geschäftsräumen der Genossenschaft zugänglich gemacht und auf der Webseite der Genossenschaft in einem nur für Mitglieder zugänglichen Bereich bekannt gemacht.

(5) Auf Verlangen und gegen Kostenerstattung kann jedes Mitglied eine Abschrift dieser Dokumente ausgehändigt bekommen.

### § 20 Bildung von Rücklagen

(1) Die Genossenschaft ist bestrebt, ihre finanzielle Lage durch Bildung von Rücklagen zu konsolidieren und faire Gehälter auszuzahlen.

(2) Es ist eine gesetzliche Rücklage zu bilden. Sie dient ausschließlich zur Deckung eines aus der Bilanz sich ergebenden Verlustes.

(3) Die gesetzliche Rücklage wird gebildet durch Zuweisung des gesamten Jahresüberschusses, bis sie XY. Über die Verwendung der gesetzlichen Rücklage beschließt die Generalversammlung.

(4) Im Übrigen können bei der Aufstellung des Jahresabschlusses weitere Ergebnisrücklagen gebildet werden. Über ihre Verwendung beschließen Vorstand und Aufsichtsrat in gemeinsamer Sitzung (XY).

### § 21 Verwendung des Jahresüberschusses

Über die Verwendung des Jahresüberschusses beschließt unter Be­achtung der Vorschriften des Gesetzes und dieser Satzung die Generalversammlung; dieser darf, soweit er nicht der gesetzlichen Rücklage oder anderen Ergebnisrücklagen (§ 20) zugeführt wird, nur für gemeinnützige Zwecke verwendet werden.

### § 22 Deckung eines Jahresfehlbetrages

(1) Über die Deckung eines Jahresfehlbetrages beschließt die Generalver­sammlung.

(2) Soweit ein Jahresfehlbetrag nicht auf neue Rechnung vorgetragen oder durch Heranzie­hung der anderen Ergebnisrücklagen gedeckt wird, ist er durch die gesetzliche Rücklage oder durch Abschreibung von den Geschäftsguthaben der Mitglieder oder durch diese Maßnahmen zugleich zu decken.

(3) Werden die Geschäftsguthaben zur Deckung eines Jahresfehlbetrages herangezogen, so wird der auf das einzelne Mitglied entfallende Anteil des Jahresfehlbetrages nach dem Verhält­nis der übernommenen Geschäftsanteile aller Mitglieder bei Beginn des Geschäftsjahres, in dem der Jahresfehlbetrag entstanden ist, be­rech­net.

### § 23 Rückvergütung

Eine Rückvergütung wird nicht gewährt.

### § 24 Auflösung und Abwicklung

(1) Die Genossenschaft wird aufgelöst

  a) durch Beschluss der Generalversammlung,
  b) durch Eröffnung des Insolvenzverfahrens,
  c) durch Beschluss des Gerichts, wenn die Zahl der Mitglieder weniger als drei beträgt.

(2) Nach Auflösung wird die Genossenschaft nach Maßgabe des Genossenschaftsgesetzes liquidiert.

(3) Für die Verteilung des Vermögens der Genossenschaft ist das Gesetz mit der Abweichung maßgebend, dass Überschüsse, welche sich über den Gesamtbetrag der Geschäftsguthaben hinaus ergeben, gemeinnützigen Zwecken zugeführt werden.

## V. Sonstiges

### § 25 Gerichtsstand

Gerichtsstand für alle Streitigkeiten zwischen einem Mitglied und der Genossenschaft aus dem Mitgliedschaftsverhältnis ist das Amtsgericht oder das Landgericht, das für den Sitz der Genos­senschaft zuständig ist.

### § 26 Bekanntmachungen

(1) Bekanntmachungen erfolgen durch unmittelbare Information der Mitglieder auf elektronischem Wege, vorrangig auf der Webseite der Genossenschaft.

(2) Soweit die Veröffentlichung gesetzlich vorgeschrieben ist, erfolgen Bekanntmachungen unter der Firma der Genossenschaft im elektronischen Bundesanzeiger.

### § 27 Schlussbestimmungen

(1) Wenn einzelne Bestimmungen dieser Satzung unwirksam sein, oder unwirksam werden sollten, so soll die Gültigkeit aller anderen Bestimmungen dadurch nicht berührt werden. Die ungültigen Bestimmungen sind durch gesetzlich zulässige Bestimmungen zu ersetzen. Die Generalversammlung hat diese Bestimmungen in ihrer nächsten Sitzung durch solche zu ersetzen, die dem wirtschaftlichen Interesse der Genossenschaft und ihrer Mitglieder am besten entsprechen.

(2) Diese Satzung ist durch die Mitglieder in der Gründungsversammlung vom XX.YY.ZZZZ beschlossen worden.

