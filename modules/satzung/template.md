$$ Überschrift
<!--
§ 6 GenG - Mindestinhalt:
  - Firma
  - Gegenstand
  - Nachschusspflicht
  - Generalversammlung
  - Bekanntmachungen
§ 7 GenG - Weiterer zwingender Inhalt:
  - Geschäftsanteil
  - Rücklage
§ 8 GenG - Satzungsvorbehalt:
  - Genossenschaft auf Zeit
  - Mitgliedschaft nach Wohnort
  - Geschäftsjahr != Kalenderjahr
  - Qualifizierte Mehrheit
  - Ausdehnung auf Nichtmitglieder
  - Investierende Mitglieder
-->

(1) Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto eligendi maiores vitae. Qui voluptas accusamus natus perspiciatis asperiores reprehenderit adipisci magni doloribus soluta cum non, deserunt magnam quo veritatis hic.

(2) Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto eligendi maiores vitae. Qui voluptas accusamus natus perspiciatis asperiores reprehenderit adipisci magni doloribus soluta cum non, deserunt magnam quo veritatis hic.

  a) Lorem ipsum dolor sit amet consectetur adipisicing elit.
  b) Lorem ipsum dolor sit amet consectetur adipisicing elit.
  c) Lorem ipsum dolor sit amet consectetur adipisicing elit.

(3) Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto eligendi maiores vitae. Qui voluptas accusamus natus perspiciatis asperiores reprehenderit adipisci magni doloribus soluta cum non, deserunt magnam quo veritatis hic.
