$$ Auseinandersetzung
<!-- § 73 GenG -->

(1) Für die Auseinandersetzung zwischen dem ausgeschiedenen Mitglied und der Genossenschaft ist der festgestellte Jahresabschluss maßgebend; Verlustvorträge sind nach dem Verhältnis der Geschäftsanteile zu berücksichtigen. Im Fall der Übertragung des Geschäftsguthabens ({{ mitgliedschaft/beendigung/uebertragung }}) findet eine Auseinandersetzung nicht statt.

(2) Dem ausgeschiedenen Mitglied ist das Auseinandersetzungsguthaben binnen sechs Monaten nach dem Ausscheiden auszuzahlen. Die Genossenschaft ist berechtigt, bei der Auseinandersetzung die ihr gegen das ausgeschiedene Mitglied zustehenden fälligen Forderungen gegen das auszuzahlende Guthaben aufzurechnen. Auf die Rücklagen und das sonstige Vermögen der Genossenschaft hat das Mitglied keinen Anspruch.

(3) Der Genossenschaft haftet das Auseinandersetzungsguthaben des Mitglieds als Pfand für einen etwaigen Ausfall, insbesondere im Insolvenzverfahren des Mitglieds.

(4) Die Absätze 1 bis 3 gelten entsprechend für die Auseinandersetzung bei der Kündigung einzelner Geschäftsanteile ({{ mitgliedschaft/beendigung/kuendigung }} Abs. 2).

<!-- TODO: Beträgt das Auseinandersetzungsguthaben mehr als XY Euro, wird es dem ausgeschiedenen Mitglied in vierteljährlichen Raten [...] ausgezahlt. -->
