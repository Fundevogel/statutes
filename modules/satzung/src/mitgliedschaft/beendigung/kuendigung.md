$$ Kündigung
<!-- § 65 GenG -->

<!-- § 65 Abs. 2 Satz 1: "mindestens drei Monate" -->
(1) Jedes Mitglied kann seine Mitgliedschaft zum Schluss eines Geschäftsjahres unter Einhaltung einer Frist von sechs Monaten schriftlich kündigen.

<!-- §§ 67b, 65 Abs. 2 Satz 1: "mindestens drei Monate" -->
<!-- TODO: "ohne hierzu durch die Satzung oder eine Vereinbarung mit der Genossenschaft verpflichtet zu sein" -->
<!-- TODO: Eselsohr: zwei Jahre? -->
(2) Ein Mitglied, das mit mehreren Geschäftsanteilen beteiligt ist, kann die Beteiligung mit einem oder mehreren seiner weiteren Geschäftsanteile zum Schluss eines Geschäftsjahres unter Einhaltung einer Frist von neun Monaten schriftlich kündigen.
