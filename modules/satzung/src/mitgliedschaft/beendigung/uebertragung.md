$$ Übertragung des Geschäftsguthabens
<!-- § 76 GenG -->

(1) Ein Mitglied kann jederzeit, auch im Laufe des Geschäftsjahres, sein Geschäftsguthaben durch schriftlichen Vertrag einem anderen übertragen und hierdurch aus der Genossenschaft ohne Auseinandersetzung ausscheiden, sofern die erwerbende Person an seiner Stelle Mitglied wird. Ist die erwerbende Person bereits Mitglied, so ist die Übertragung des Geschäftsguthabens nur zulässig, sofern sein bisheriges Geschäftsguthaben nach Zuschreibung des Geschäftsguthabens des Veräußerers den zulässigen Gesamtbetrag der Geschäftsanteile, mit denen die erwerbende Person beteiligt ist oder sich beteiligt, nicht übersteigt.

(2) Ein Mitglied kann sein Geschäftsguthaben, ohne aus der Genossenschaft auszuscheiden, teilweise übertragen und damit die Anzahl seiner Geschäftsanteile verringern. Absatz 1 gilt entsprechend.

<!-- TODO: Nach Gesetzeswortlaut MUSS das nicht der Vorstand sein -->
(3) Die Übertragung des Geschäftsguthabens bedarf außer in den Fällen des § 76 Absatz 2 Genossenschaftsgesetz der Zustimmung des Vorstands.
