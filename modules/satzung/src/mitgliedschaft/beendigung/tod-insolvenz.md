$$ Tod und Insolvenz
<!-- § 77 GenG -->

(1) Im Falle des Todes eines Mitglieds wird dessen Mitgliedschaft in der Genossenschaft durch dessen Erb:in fortgesetzt. Für den Fall der Beerbung durch mehrere Erb:innen endet die Mitgliedschaft, wenn sie nicht bis zum Ablauf des auf den Erbfall folgenden Jahres eine:r Miterb:in allein überlassen worden ist. Mehrere Erb:innen können das Stimmrecht in der Generalversammlung nur durch einen gemeinschaftliche:n Vertreter:in ausüben.

<!-- vgl. § 66a GenG -->
(2) Wird über das Vermögen eines Mitglieds ein Insolvenzverfahren eröffnet oder die Eröffnung eines Insolvenzverfahrens mangels Masse abgelehnt, so endet die Mitgliedschaft mit dem Schluss des Geschäftsjahres, in dem das Insolvenzverfahren eröffnet oder die Eröffnung mangels Masse abgelehnt wurde.
