$$ Ausschluss
<!-- § 68 GenG -->

(1) Ein Mitglied kann aus der Genossenschaft zum Schluss des Geschäftsjahres ausgeschlossen werden, wenn die Voraussetzungen für die Aufnahme in die Genossenschaft nicht vorhanden waren oder nicht mehr vorhanden sind oder wenn sich das Verhalten des Mitglieds mit den Belangen der Genossenschaft nicht vereinbaren lässt. Dies ist insbesondere dann der Fall, wenn es

  a) trotz schriftlicher Aufforderung unter Androhung des Ausschlusses den satzungsmäßigen oder sonstigen der Genossenschaft gegenüber bestehenden Verpflichtungen nicht nachkommt,
  b) unrichtige Jahresabschlüsse oder Vermögensübersichten einreicht oder sonst unrichtige oder unvollständige Erklärungen über seine rechtlichen oder wirtschaftlichen Verhältnisse abgibt,
  c) durch Nichterfüllung seiner Verpflichtungen gegenüber der Genossenschaft diese schädigt oder geschädigt hat,
  d) ein eigenes, mit der Genossenschaft im Wettbewerb stehendes Unternehmen betreibt oder sich an einem solchen beteiligt oder wenn ein mit der Genossenschaft im Wettbewerb stehendes Unternehmen sich an dem Unternehmen des Mitglieds beteiligt oder
  e) unter der der Genossenschaft bekannt gegebenen Anschrift dauernd nicht erreichbar ist.

(2) Über den Ausschluss entscheidet der Vorstand mit Zustimmung des Aufsichtsrates. Mitglieder des Vorstandes und des Aufsichtsrates können jedoch nur durch Beschluss der Generalversammlung ausgeschlossen werden.

(3) Vor der Beschlussfassung ist dem Auszuschließenden Gelegenheit zu geben, sich zu dem beabsichtigten Ausschluss zu äußern. Hierbei sind ihm die wesentlichen Tatsachen, auf denen der Ausschluss beruhen soll, sowie der satzungsmäßige Ausschließungsgrund mitzuteilen.

(4) Die ausgeschlossene Person kann, wenn nicht die Generalversammlung den Ausschluss beschlossen hat, innerhalb eines Monats seit der Absendung des Briefes Beschwerde beim Aufsichtsrat einlegen; in diesem Fall entscheidet die Generalversammlung über den Ausschluss. Legt die ausgeschlossene Person nicht fristgerecht Beschwerde ein, ist der ordentliche Rechtsweg ausgeschlossen.
