$$ Rechte der Mitglieder

(1) Alle Mitglieder haben, unabhängig von ihren Geschäftsanteilen, eine Stimme in der Generalversammlung und der Online-Generalversammlung.

(2) Jedes Mitglied hat das Recht, die Einrichtungen der Genossenschaft nach Maßgabe der dafür getroffenen Bestimmungen zu benutzen und an der Gestaltung der Genossenschaft mitzuwirken.
