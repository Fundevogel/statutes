$$ Erwerb der Mitgliedschaft

(1) Die Mitgliedschaft können erwerben:

  <!-- TODO: Ergänzung, das Mitglied müsse die Grundsätze der Genossenschaft teilen? -->
  a) natürliche Personen,
  b) Personengesellschaften,
  c) juristische Personen des privaten oder öffentlichen Rechts.

(2) Aufnahmefähig ist nur, wer die Voraussetzungen für die Inanspruchnahme der Einrichtungen der Genossenschaft erfüllt oder dessen Mitgliedschaft im Interesse der Genossenschaft liegt. Aufnahmefähig ist nicht, wer bereits Mitglied einer anderen Vereinigung ist, die im Wesentlichen gleichartige Geschäfte betreibt, oder wer derartige Geschäfte selbst betreibt oder betreiben lässt.

<!-- §§ 15 ff. GenG -->
(3) Die Mitgliedschaft wird erworben durch

  a) eine von dem Beitretenden zu unterzeichnende unbedingte Erklärung des Beitritts und
  b) die Zulassung durch das Betriebsplenum.

Lehnt der Vorstand die Aufnahme ab, so steht der abgewiesenen Person innerhalb eines Monats nach der Benachrichtigung das Recht der Berufung an den Aufsichtsrat zu, der endgültig entscheidet.
