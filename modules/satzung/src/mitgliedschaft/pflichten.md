$$ Pflichten der Mitglieder

Jedes Mitglied ist verpflichtet,

  a) die Interessen der Genossenschaft zu wahren und den Bestimmungen des Genossenschaftsgesetzes, der Satzung und den Beschlüssen der Generalversammlung nachzukommen,
  b) die Einzahlung auf den Geschäftsanteil zu leisten oder auf weitere Geschäftsanteile gemäß {{ mitgliedschaft/geschaeftsanteil }} Abs. 3 zu leisten,
  c) Adressenänderungen innerhalb von drei Wochen dem Vorstand mitzuteilen und
  d) interne Informationen und Vorgänge sowie sonstige Angelegenheiten, die der Genossenschaft erheblichen Schaden zufügen können, nicht an unbeteiligte Dritte weiterzugeben.
