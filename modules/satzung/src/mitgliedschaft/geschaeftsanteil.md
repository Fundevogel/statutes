$$ Geschäftsanteil
<!-- § 7 Nr. 1 GenG -->

<!-- TODO: Beachte § 7a Abs. 2 S. 2 im Falle einer Pflichtbeteiligung! -->
(1) Der Geschäftsanteil beträgt {{ HOEHE_ANTEIL }} Euro und ist bei Eintritt an die Genossenschaft zu zahlen. Jedes Mitglied, das juristische Person ist, sollte wenigstens {{ MIN_ANTEILE_ORGS }} Geschäftsanteile erwerben.

<!-- § 7a Abs. 3 GenG -->
(2) Sacheinlagen können mit Zustimmung des Betriebsplenums als Einzahlung auf den Geschäftsanteil zugelassen werden.

<!-- § 7a Abs. 1 GenG -->
<!-- TODO: Zustimmung des Vorstands diskutieren -->
<!-- TODO: Siehe § 15b Abs. 2 GenG: Sinngemäße Wiederholung des Gesetzes!  -->
(3) Ein Mitglied kann sich mit weiteren Geschäftsanteilen an der Genossenschaft betei­ligen; für den Erwerb gilt {{ mitgliedschaft/erwerb }} entsprechend. Falls ein Mitglied nach dem Erwerb weiterer Geschäftsanteile {{ MAX_ANTEILE_PROZENT }} % aller Geschäftsanteile besitzen würde, bedarf der Erwerb der Zustimmung des Aufsichtsrates.
