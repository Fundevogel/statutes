$$ Beendigung der Mitgliedschaft

Die Mitgliedschaft endet durch

  a) Kündigung ({{ mitgliedschaft/beendigung/kuendigung }} Abs. 1),
  b) Ausschluss ({{ mitgliedschaft/beendigung/ausschluss }}).
  c) Übertragung des Geschäftsguthabens ({{ mitgliedschaft/beendigung/uebertragung }} Abs. 1),
  d) Tod ({{ mitgliedschaft/beendigung/tod-insolvenz }} Abs. 1) oder Insolvenz eines Mitglieds ({{ mitgliedschaft/beendigung/tod-insolvenz }} Abs. 2),
  e) Auflösung einer juristischen Person oder Personengesellschaft (§ 77a GenG) oder
