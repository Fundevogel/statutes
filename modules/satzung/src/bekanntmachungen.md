$$ Bekanntmachungen
<!-- § 6 Nr. 5, 156 GenG -->

(1) Bekanntmachungen erfolgen durch unmittelbare Information der Mitglieder auf elektronischem Wege, vorrangig auf der Webseite der Genossenschaft.

<!-- TODO: Alternativ: Verbandswebseiten oä -->
(2) Soweit die Veröffentlichung gesetzlich vorgeschrieben ist, erfolgen Bekanntmachungen unter der Firma der Genossenschaft im elektronischen Bundesanzeiger.
