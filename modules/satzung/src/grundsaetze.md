$$ Grundsätze

<!-- Vorlage "Wigwam" (Berlin): -->
(1) Die Genossenschaft soll sich in Projektarbeit und internen Prozessen für eine Welt einsetzen, in der Menschen ein wechselseitiges Bewusstsein füreinander haben, im Diskurs miteinander stehen und ihr zukünftiges Handeln entsprechend danach ausrichten. Insbesondere soll sie soziales, ökologisches und nachhaltiges Engagement fördern.

(2) Die Genossenschaft soll die ökologischen und sozialen Wirkungen ihrer Geschäftstätigkeit und ihres Umgangs mit den Mitgliedern erfassen, negative Wirkungen systematisch vermindern und positive Wirkungen fördern und gegenüber den Mitgliedern Transparenz herstellen. So wird das Ziel verfolgt, im Sinne der Genossenschaft und des Gemeinwohls zu wirtschaften. Finanzieller Gewinn steht nicht an erster Stelle, sondern ist ein Mittel zum Zweck eines unternehmerischen Gemeinwohlbeitrages.

(3) Die Genossenschaft soll bei der Wahl ihrer Geschäfts- und Kooperationspartner:innen darauf achten, dass die vorgenannten Grundsätze auch bei diesen gelten. Bei Unschlüssigkeit darüber muss das Thema unabhängig und neutral in der Genossenschaft zur Diskussion und Entscheidung gestellt werden.
<!-- TODO: OpenSource und Freie Software, freier Zugang zu Informationen / Wissen -->
