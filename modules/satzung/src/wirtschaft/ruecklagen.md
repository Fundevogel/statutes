$$ Bildung von Rücklagen

<!-- § 7 Nr. 2 GenG -->
<!-- TODO: Optional -->
(1) Die Genossenschaft ist bestrebt, ihre finanzielle Lage durch Bildung von Rücklagen zu konsolidieren und faire Gehälter auszuzahlen.

<!-- Konsistenz: "eines Jahresfehlbetrages" -->
(2) Es ist eine gesetzliche Rücklage zu bilden. Sie dient ausschließlich zur Deckung eines aus der Bilanz sich ergebenden Verlustes.

<!-- TODO: Höhe ergänzen -->
<!-- mindes­tens 100 % der Summe der Geschäftsanteile / der Bilanzsumme -->
<!-- XY % aller Lohnkosten der Genossenschaft (einschließlich sozialer Abgaben und Aufwendungen) des Dezembers des vorhergegangenen Geschäftsjahres -->
<!-- XY % der Bilanzsumme / "in der Jahresbilanz ausgewiesenen Verbindlichkeiten" -->
<!-- TODO: Ggf. "zuzüglich eines Gewinnvortrages" bzw "abzüglich eines Verlustvortrages" bzw beides -->
<!-- TODO: Ggf. "Die gesetzliche Rücklage ist bei der Aufstellung des Jahresabschlusses zu bilden" -->
(3) Die gesetzliche Rücklage wird gebildet durch Zuweisung des gesamten Jahresüberschusses, bis sie XY. Über die Verwendung der gesetzlichen Rücklage beschließt die Generalversammlung.

<!-- TODO: Norm ergänzen -->
(4) Im Übrigen können bei der Aufstellung des Jahresabschlusses weitere Ergebnisrücklagen gebildet werden. Über ihre Verwendung beschließen Vorstand und Aufsichtsrat in gemeinsamer Sitzung (XY).
