$$ Auflösung und Abwicklung

(1) Die Genossenschaft wird aufgelöst

  a) durch Beschluss der Generalversammlung,
  b) durch Eröffnung des Insolvenzverfahrens,
  c) durch Beschluss des Gerichts, wenn die Zahl der Mitglieder weniger als drei beträgt.

(2) Nach Auflösung wird die Genossenschaft nach Maßgabe des Genossenschaftsgesetzes liquidiert.

(3) Für die Verteilung des Vermögens der Genossenschaft ist das Gesetz mit der Abweichung maßgebend, dass Überschüsse, welche sich über den Gesamtbetrag der Geschäftsguthaben hinaus ergeben, gemeinnützigen Zwecken zugeführt werden.
