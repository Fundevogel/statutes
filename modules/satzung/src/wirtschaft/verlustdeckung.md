$$ Deckung eines Jahresfehlbetrages
<!-- § 19 GenG: Gewinn- und Verlustverteilung -->
<!-- Alternativ: "Verlustdeckung" -->

(1) Über die Deckung eines Jahresfehlbetrages beschließt die Generalver­sammlung.

(2) Soweit ein Jahresfehlbetrag nicht auf neue Rechnung vorgetragen oder durch Heranzie­hung der anderen Ergebnisrücklagen gedeckt wird, ist er durch die gesetzliche Rücklage oder durch Abschreibung von den Geschäftsguthaben der Mitglieder oder durch diese Maßnahmen zugleich zu decken.

(3) Werden die Geschäftsguthaben zur Deckung eines Jahresfehlbetrages herangezogen, so wird der auf das einzelne Mitglied entfallende Anteil des Jahresfehlbetrages nach dem Verhält­nis der übernommenen Geschäftsanteile aller Mitglieder bei Beginn des Geschäftsjahres, in dem der Jahresfehlbetrag entstanden ist, be­rech­net.
