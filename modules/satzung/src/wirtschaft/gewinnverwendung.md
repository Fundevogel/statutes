$$ Verwendung des Jahresüberschusses
<!-- §§ 19, 20 GenG: Gewinn- und Verlustverteilung; Ausschluss der Gewinnverteilung -->
<!-- Alternativ: "Gewinnverwendung" -->

<!-- TODO: Norm nachtragen -->
Über die Verwendung des Jahresüberschusses beschließt unter Be­achtung der Vorschriften des Gesetzes und dieser Satzung die Generalversammlung; dieser darf, soweit er nicht der gesetzlichen Rücklage oder anderen Ergebnisrücklagen ({{ wirtschaft/ruecklagen }}) zugeführt wird, nur für gemeinnützige Zwecke verwendet werden.
