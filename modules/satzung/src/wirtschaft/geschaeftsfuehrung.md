$$ Geschäftsführung

(1) Das Geschäftsjahr ist das Kalenderjahr.

(2) Der Vorstand stellt innerhalb von fünf Monaten nach Ende des Geschäftsjahres den Jahresabschluss und ggf. den Lagebericht für das vergangene Jahr Geschäftsjahr auf, und legt sie dem Aufsichtsrat zur Prüfung vor.

(3) Jahresabschluss und ggf. Lagebericht werden auf der Generalversammlung mit den Bemerkungen des Aufsichtsrates vorgelegt. Der Aufsichtsrat berichtet auf der Generalversammlung über seine Prüfung des Jahresabschlusses und des Lageberichts.

(4) Zwei Wochen vor der Generalversammlung werden der Jahresabschluss, ggf. Lagebericht
und der Bericht des Aufsichtsrates in den Geschäftsräumen der Genossenschaft zugänglich gemacht und auf der Webseite der Genossenschaft in einem nur für Mitglieder zugänglichen Bereich bekannt gemacht.

(5) Auf Verlangen und gegen Kostenerstattung kann jedes Mitglied eine Abschrift dieser Dokumente ausgehändigt bekommen.
