$$ Schlussbestimmungen

(1) Wenn einzelne Bestimmungen dieser Satzung unwirksam sein, oder unwirksam werden sollten, so soll die Gültigkeit aller anderen Bestimmungen dadurch nicht berührt werden. Die ungültigen Bestimmungen sind durch gesetzlich zulässige Bestimmungen zu ersetzen. Die Generalversammlung hat diese Bestimmungen in ihrer nächsten Sitzung durch solche zu ersetzen, die dem wirtschaftlichen Interesse der Genossenschaft und ihrer Mitglieder am besten entsprechen.

(2) Diese Satzung ist durch die Mitglieder in der Gründungsversammlung vom {{ DATUM_GRUENDUNG }} beschlossen worden.
