$$ Generalversammlung

(1) Die Generalversammlung wird vom Vorstand durch unmittelbare Benachrichtigung sämtlicher Mitglieder in Textform einberufen. Bei der Einberufung ist die Tagesordnung bekannt zu machen. Die Einladung muss mindestens zwei Wochen, Ergänzungen und Änderungen der Tagesordnung müssen mindestens eine Woche vor der Generalversammlung erfolgen. Die Mitteilungen gelten als zugegangen, wenn sie zwei Werktage vor Beginn der Frist abgesendet worden sind.

(2) Jede ordnungsgemäß einberufene Generalversammlung ist (unabhängig von der Zahl der Teilnehmer:innen) beschlussfähig.

(3) Jedes Mitglied hat eine Stimme.

(4) Die Generalversammlung bestimmt die Versammlungsleitung auf Vorschlag des Aufsichtsrates.

(5) Abstimmungen und Wahlen erfolgen in der Generalversammlung durch offene Abstimmung.

(6) Im Rahmen der Regelungen in § 43 Abs. 7 GenG haben die Mitglieder ein Recht auf Online-Teilnahme an der Generalversammlung und Beteiligung an der Fassung der Beschlüsse in elektronischer Form. Hierbei ist zu gewährleisten, dass die Identität des Mitglieds zweifelsfrei überprüft wird. Über die Möglichkeit der Online-Teilnahme ist in der Ladung zur Generalversammlung hinzuweisen. Sofern Beschlussfassungen geheim durchgeführt werden, sind geeignete Maßnahmen zu ergreifen, um eine anonymisierte Stimmabgabe bei Online-Teilnahme zu gewährleisten.

(7) Die Generalversammlung darf keine Gewinnverteilung an die Mitglieder beschließen.

(8) Beschlüsse werden gemäß § 47 GenG protokolliert.
