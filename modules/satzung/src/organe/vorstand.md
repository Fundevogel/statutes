$$ Vorstand
<!-- §§ 9, 24 ff. GenG -->

<!-- § 24 Abs. 3 S. 1 GenG -->
(1) Der Vorstand besteht aus mindestens {{ ANZAHL_VORSTAND_MIN }}, höchstens {{ ANZAHL_VORSTAND_MAX }} Mitgliedern und arbeitet ehrenamtlich.

(2) Je zwei Mitglieder des Vorstands vertreten gemeinschaftlich die Genossenschaft gerichtlich und außergerichtlich. Die Genossenschaft kann auch durch ein Vorstandsmitglied in Gemeinschaft mit einem Prokuristen gesetzlich vertreten werden.
<!-- Der Aufsichtsrat kann einzelne oder alle Vorstandsmitglieder von dem Verbot der Mehrvertretung gemäß § 181 Alternative 2 BGB befreien. -->

(3) Die Vorstandsmitglieder werden vom Aufsichtsrat aus der Mitte der momentan Beschäftigten der Genossenschaft bestellt und abberufen. Den Vorsitzenden des Vorstands und dessen Stellvertreter wählt der Vorstand aus seiner Mitte.

(4) Die Amtzeit der Vorstandsmitglieder beträgt {{ AMTSZEIT_VORSTAND }} Jahre.

(5) Der Vorstand bedarf im Innenverhältnis der Zustimmung des Betriebsplenums ({{ organe/betriebsplenum }}) für

  a) den Abschluss von Verträgen mit wiederkehrenden Verpflichtungen mit einer Laufzeit von mehr als zwei Jahren bzw. einer jährlichen Belastung von mehr als 2.000 Euro,
  b) Investitionen bzw. die Aufnahme von Krediten mit einem Gesamtvolumen von mehr als 5.000 Euro,
  c) die Aufnahme, Übertragung oder Aufgabe eines wesentlichen Geschäftsbereichs, soweit nicht die Generalversammlung zuständig ist,
  d) den Beitritt zu und Austritt aus Verbänden und sonstigen Vereinigungen sowie
  f) die Erteilung von Prokura.
