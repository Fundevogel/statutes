$$ Mehrheitserfordernisse

(1) Entscheidungen der Genossenschaft sollen stets im Bemühen um Konsens erfolgen.

(2) Die Generalversammlung beschließt mit der Mehrheit der abgegebenen Stimmen (einfache Stimmenmehrheit), soweit keine größere Mehrheit bestimmt ist; Stimmenthaltungen bleiben unberücksichtigt. Gibt es bei einer Wahl mehr Bewerber:innen als Mandate vorhanden sind, so hat jede:r Wahlberechtigte so viele Stimmen, wie Mandate zu vergeben sind. Es sind diejenigen Bewerber:innen gewählt, die die meisten Stimmen auf sich vereinigen (relative Mehrheit).

(3) Eine Mehrheit von drei Vierteln der gültig abgegebenen
Stimmen ist insbesondere in folgenden Fällen erforderlich:

  a) Änderung der Satzung;
  b) Ausschluss von Vorstands- und Aufsichtsratsmitgliedern aus der Genossenschaft;
  c) Austritt aus genossenschaftlichen Verbänden;
  d) Verschmelzung oder Auflösung der Genossenschaft sowie die Fortsetzung der Genossenschaft nach beschlossener Auflösung.

(4) Ein Beschluss über die Änderung der Rechtsform bedarf
der Mehrheit von neun Zehnteln der gültig abgegebenen
Stimmen.

(5) Bei der Beschlussfassung über die Auflösung, die Änderung der Rechtsform und bei der Wahl und Abberufung des Vorstands müssen über die gesetzlichen Vorschriften hinaus 60% aller Mitglieder anwesend sein. Wenn diese Mitgliederzahl in der Versammlung nicht erreicht ist, ist jede weitere Versammlung ohne Rücksicht auf die Zahl der erschienenen Mitglieder innerhalb desselben Geschäftsjahres beschlussfähig.

(6) Die Absätze 3 und 4 können nur mit der in Absatz 3 genannten Mehrheit geändert werden.
