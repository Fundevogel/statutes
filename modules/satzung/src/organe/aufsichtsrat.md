$$ Aufsichtsrat

(1) Der Aufsichtsrat besteht aus mindestens {{ ANZAHL_AUFSICHTSRAT_MIN }}, höchstens {{ ANZAHL_AUFSICHTSRAT_MAX }} Mitgliedern, die von der Generalversammlung gewählt werden. Er wird einzeln vertreten vom Vorsitzenden oder dessen Stellvertreter.

(2) Die Wahl der Aufsichtsratsmitglieder erfolgt mit der Mehrheit der Stimmen der anwesenden Mitglieder; Stimmenthaltungen gelten als ungültig und werden nicht mitgezählt. Erhalten mehr Bewerber die erforderliche Mehrheit, als Sitze im Aufsichtsrat zu besetzen sind, so sind die Bewerber mit den meisten Stimmen gewählt.

(3) Das Amt eines Aufsichtsratsmitglieds beginnt mit dem Schluss der Generalversammlung, die die Wahl vorgenommen hat, und endet am Schluss der Generalversammlung, die für das dritte Geschäftsjahr nach der Wahl stattfindet. Hierbei wird das Geschäftsjahr, in welchem das Aufsichtsratsmitglied gewählt wird, mitgerechnet. Die Generalversammlung kann für alle oder einzelne Aufsichtsratsmitglieder eine kürzere Amtsdauer bestimmen. Wiederwahl ist zulässig.

(4) Die Wahl zum Mitglied des Aufsichtsrats kann vor dem Ende der Amtszeit durch die Generalversammlung widerrufen werden. Der Beschluss bedarf einer Mehrheit von drei Vierteln der abgegebenen Stimmen.

(5) Der Aufsichtsrat wählt im Anschluss an jede Wahl von Aufsichtsratsmitgliedern aus seiner Mitte einen Vorsitzenden sowie einen Stellvertreter.

(6) Der Aufsichtsrat ist beschlussfähig, wenn mehr als die Hälfte seiner Mitglieder, darunter der Vorsitzende oder sein Stellvertreter, anwesend ist. Eine Beschlussfassung ist in dringenden Fällen auch ohne Einberufung einer Sitzung im Wege schriftlicher Abstimmung oder durch entsprechende Fernkommunikationsmedien zulässig, wenn der Vorsitzende des Aufsichtsrats oder sein Stellvertreter eine solche Beschlussfassung veranlasst und kein Mitglied des Aufsichtsrats diesem Verfahren widerspricht.

(7) Der Aufsichtsrat hat den Vorstand bei dessen Geschäftsführung zu überwachen und sich zu diesem Zweck über die Angelegenheiten der Genossenschaft zu unterrichten. Er kann jederzeit Berichterstattung vom Vorstand verlangen und selbst oder durch einzelne von ihm zu bestimmende Mitglieder die Bücher und Schriften der Genossenschaft sowie den Kassenbestand und die Bestände an Wertpapieren, Handelspapieren und Waren einsehen und prüfen. Auch ein einzelnes Mitglied des Aufsichtsrats kann Auskünfte, jedoch nur an den Aufsichtsrat, verlangen.
