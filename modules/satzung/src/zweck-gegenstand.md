$$ Zweck und Gegenstand
<!-- Gegenstand: § 6 Nr. 2 -->

<!-- § 1 Abs. 1 GenG: "deren Zweck darauf gerichtet ist, den Erwerb oder die Wirtschaft ihrer Mitglieder oder deren soziale oder kulturelle Belange durch gemeinschaftlichen Geschäftsbetrieb zu fördern" -->
(1) Die Genossenschaft bezweckt die wirtschaftliche Förderung und Betreuung der Mitglieder mittels gemeinschaftlichen Geschäftsbetriebes; dabei steht Kinder- und Jugendliteratur im Mittelpunkt.

(2) Gegenstand der Genossenschaft ist der Erwerb und Vertrieb von Druckerzeugnissen, Bild-, Ton- und sonstigen Datenträgern sowie Medien aller Art. Die Genossenschaft verwirklicht ihren satzungsmäßigen Zweck insbesondere durch

  a) Fortführung der Freiburger Kinder- und Jugendbuchhandlung Fundevogel,
  b) Durchführung von Veranstaltungen mit Autor:innen und Verlagen,
  d) Herausgabe von Empfehlungslisten mit ausgewählten Neuerscheinungen,
  c) Angebote der Leseförderung und -forderung junger Menschen sowie
  e) Fortbildungsmaßnahmen im Bereich der frühkindlichen Bildung.

(3) Die Genossenschaft strebt zur Erfüllung der vorgenannten Ziele die Zusammenarbeit und Vernetzung mit gleichgesinnten Kooperationspartner:innen an. Außerdem unterstützt die Genossenschaft die Mitglieder bei der Weiterbildung in den zuvor genannten Bereichen.

<!-- § 1 Abs. 2 Nr. 2 GenG: Die Beteiligung einer Genossenschaft an einem anderen Unternehmen darf nie
Hauptzeck der Genossenschaft sein, da ansonsten die Regelungen des KAGB greifen (zB Registrierungs- &
Meldepflichten bei der BaFin). -->
<!-- zur Zweigniederlassung, vgl. § 14 GenG -->
(4) Die Genossenschaft ist berechtigt, andere Unternehmen zu errichten und zu erwerben sowie sich an anderen Unternehmen zu beteiligen, wenn dies der Förderung der Mitglieder dient. Sie ist ferner berechtigt, Zweigniederlassungen zu errichten.

<!-- Zulassung Nichtmitglieder: § 8 Abs. 1 Nr. 5 GenG -->
(5) Die Ausdehnung des Geschäftsbetriebes auf Nichtmitglieder ist zugelassen.
