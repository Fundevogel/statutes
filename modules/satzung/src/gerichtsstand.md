$$ Gerichtsstand

Gerichtsstand für alle Streitigkeiten zwischen einem Mitglied und der Genossenschaft aus dem Mitgliedschaftsverhältnis ist das Amtsgericht oder das Landgericht, das für den Sitz der Genos­senschaft zuständig ist.
