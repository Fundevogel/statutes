$$ Firma und Sitz
<!-- § 6 Nr. 1 GenG -->

<!-- Zusatz "eG": § 3 GenG -->
(1) Die Firma der Genossenschaft lautet Fundevogel eG.

(2) Die Genossenschaft hat ihren Sitz in Freiburg.
