def create_path(path: str) -> None:
    """
    Creates directory
    """

    # Import modules
    import os

    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))

        # Guard against race condition
        except OSError:
            pass


def replace_references(string: str, index: dict, variables: dict) -> str:
    """
    Replaces internal variables & norm references
    """

    # Import modules
    import re

    def replace_links(match: re.Match):
        """
        Replaces single variable OR norm reference
        """

        if match.group(1) in index:
            return '§ {}'.format(index[match.group(1)])

        if match.group(1) in variables:
            return '{}'.format(variables[match.group(1)])

        return match.group(1)


    return re.sub(r'\{\{\s?(.+?)\s?\}\}', replace_links, string)


def int2roman(number: int) -> str:
    """
    Converts integers to roman literals
    """

    # Roman numerals
    numerals = {
        1: 'I',
        4: 'IV',
        5: 'V',
        9: 'IX',
        10: 'X',
        40: 'XL',
        50: 'L',
        90: 'XC',
        100: 'C',
        400: 'CD',
        500: 'D',
        900: 'CM',
        1000: 'M',
    }

    # Start with empty string
    string = ''

    # Subtract & add correct numeral until reaching the end
    for integer, numeral in sorted(numerals.items(), reverse=True):
        while number >= integer:
            string += numeral
            number -= integer

    return string


def load_yaml(file_path: str) -> dict:
    """
    Loads YAML file
    """

    # Import modules
    from yaml import safe_load

    with open(file_path, 'r') as file:
        try:
            return safe_load(file)

        except Exception:
            pass

    return {}
